"""Link station power to device problem solver

This script solves the following problem:

For a device positioned at [x1,y1] find which,
if any, link station [x2,y2,r] with reach r,
would provide most power. Where power = (reach - distance)^2.
Assuming that if distance > reach then power = 0.
"""
from typing import List, NamedTuple, Optional
from dataclasses import dataclass
from math import sqrt
from time import sleep


class Point(NamedTuple):
    """Represents 2D space coordinates"""

    x: int
    y: int


@dataclass
class Station:
    """Represents a link station located at Point(x,y) and certain reach"""

    location: Point
    reach: int

    def euclidean_distance(self, b: Point) -> float:
        """Calculates distance between station and point b in 2D space"""
        a = self.location
        return sqrt((a.x - b.x) ** 2 + (a.y - b.y) ** 2)

    def power_at_point(self, point: Point) -> float:
        """Calculates power available to a device located at given point"""
        distance = self.euclidean_distance(point)
        return ((self.reach - distance) ** 2) if (distance < self.reach) else 0


@dataclass
class Device:
    """Represents a device located at Point(x,y)"""

    location: Point


def find_best_station(stations: List[Station], device: Device) -> Optional[Station]:
    """
    Returns Station with most power available to device at point
    or None if no station could deliver power
    """

    best_station = max(stations, key=lambda s: s.power_at_point(device.location))
    return best_station if best_station.power_at_point(device.location) > 0 else None


def solve_demo() -> None:
    """
    Solves the link station power to device problem with example data
    """

    devices = [
        Device(Point(0, 0)),
        Device(Point(100, 100)),
        Device(Point(15, 10)),
        Device(Point(18, 18)),
    ]
    stations = [
        Station(Point(0, 0), 10),
        Station(Point(20, 20), 5),
        Station(Point(10, 0), 12),
    ]
    best_stations = [find_best_station(stations, x) for x in devices]
    devices_power_at_station = [
        best_stations[x].power_at_point(devices[x].location) if best_stations[x] is not None else None
        for x in range(len(devices))
    ]
    print("Program will solve problem of finding a station with most power available to a device at point [x,y]")
    sleep(1.5)

    print("Stations are located at:")
    [print(s.location) for s in stations]
    sleep(1.5)

    print("Devices are located at:")
    [print(d.location) for d in devices]
    sleep(1.5)

    for k in range(len(devices)):
        if best_stations[k] is not None:
            print(
                f"Best link station for device at {devices[k].location} is located at "
                f"{best_stations[k].location} with power {devices_power_at_station[k]}"
            )
        else:
            print(f"No link station within reach for device at {devices[k].location}")


if __name__ == "__main__":
    solve_demo()
    input("Press Enter to exit...")
