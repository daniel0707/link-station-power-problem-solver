# Link station powering device problem solver

Given a device with coordinates [x,y] and an array of link stations with coordinates and reach [x,y,r], the program will output following line:  
`Best link station for point x,y is x,y with power z`
or:  
`No link station within reach for point x,y`

## Installation

Requires Python=3.8 to run and pipenv to build.

#### Self-build

Clone the repository to local folder.
From within the project directory run

````sh
pipenv install --dev
pyinstaller -F solver/solver.py -n solver
````

This will install the dependencies required to build the program and then package it into an executable file available at `dist` folder.

#### Just execute

You can use the code as is by cloning the repository and executing from within the project directory

````sh
Python solver.py
````

#### Download

For your convienience the executable from latest build for Windows machines is available [here](https://gitlab.com/daniel0707/link-station-power-problem-solver/-/jobs/artifacts/main/download?job=BuildJob). It does not require Python installed to run.

## Usage example

Run built executable for example script usage or see `solve_demo` function in solver.py.

Can be used as module. Use included classess and `find_best_station` function for solving the problem.

## Development setup

Requires Python=3.8 and pipenv

Clone the repository and install dependencies with pipenv

```sh
git clone git@gitlab.com:daniel0707/link-station-power-problem-solver.git
cd link-station-power-problem-solver
pipenv install --dev
```

Configure linter to line-length=120

Ensure all tests are passing with

```sh
pipenv run Python -m unittest .\test_solver.py
```

before submitting a pull request.

## Meta

Daniel Vahla

Distributed under the MIT license. See ``LICENSE`` for more information.

[GitLab link](https://gitlab.com/daniel0707/link-station-power-problem-solver)

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
