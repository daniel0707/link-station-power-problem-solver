import sys
import os
import unittest

# Setup test location for imports to succeed
testdir = os.path.dirname(__file__)
srcdir = "./solver"
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

from solver import Point, Station, Device, find_best_station


class TestStation(unittest.TestCase):
    def setUp(self) -> None:
        self.station = Station(Point(10, 10), 10)

    def test_euclidean_distance_positive_point(self):
        """Test euclidean_distance method calculation for positive coordinates"""
        p = Point(40, 50)
        result = self.station.euclidean_distance(p)

        self.assertAlmostEqual(result, 50)

    def test_euclidean_distance_negative_point(self):
        """Test euclidean_distance method calculation for negative coordinates"""
        p = Point(-20, -30)
        result = self.station.euclidean_distance(p)

        self.assertAlmostEqual(result, 50)

    def test_euclidean_distance_same_point(self):
        """Test euclidean_distance method calculation for station coordinates"""
        result = self.station.euclidean_distance(self.station.location)

        self.assertEqual(result, 0)

    # assuming euclidean distance calculation is correct at this point

    def test_power_at_point_in_distance(self):
        """Test power_at_point method with point in reach"""
        p = Point(15, 10)
        result = self.station.power_at_point(p)

        self.assertEqual(result, 25)

    def test_power_at_point_at_reach_distance(self):
        """Test power_at_point method with point at reach distance exactly"""
        p = Point(20, 10)
        result = self.station.power_at_point(p)

        self.assertEqual(result, 0)

    def test_power_at_point_out_of_reach(self):
        """Test power_at_point method with point outside of reach"""
        p = Point(100, 100)
        result = self.station.power_at_point(p)

        self.assertEqual(result, 0)


class TestFindBestStation(unittest.TestCase):
    def setUp(self) -> None:
        self.stations = [
            Station(Point(0, 0), 20),
            Station(Point(20, 20), 20),
            Station(Point(10, 10), 10),
        ]

    def test_find_best_station_in_reach_all(self):
        """Test find_best_station method for a device in reach of all stations"""
        d = Device(Point(10, 10))
        result = find_best_station(self.stations, d)

        self.assertIs(result, self.stations[2])

    def test_find_best_station_in_reach_one(self):
        """Test find_best_station_method for a device in reach of one station"""
        d = Device(Point(-10, 0))
        result = find_best_station(self.stations, d)

        self.assertIs(result, self.stations[0])

    def test_find_best_station_in_reach_none(self):
        """Test find_best_station method for a device not in reach of any station"""
        d = Device(Point(100, 100))
        result = find_best_station(self.stations, d)

        self.assertIsNone(result)
